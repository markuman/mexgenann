#include <mex.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "genann.h"
#include "mexgenann.h"

//#define DEBUG

const char * retval[] =  {
	"guess"
};

typedef enum {
	ANN_STRUCT,
	DATA_INPUT
} input_arguments;

char error_string[2000];
int n;

void mexFunction (int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[]){

	// input handling
	if(nrhs != 2) {
		mexErrMsgTxt("Two inputs are required.");
	}
			
	// input parsing
	mxArray *inputs_field = mxGetField(prhs[ANN_STRUCT], 0, fieldnames[INPUTS]);
	mxArray *hidden_layers_field = mxGetField(prhs[ANN_STRUCT], 0, fieldnames[HIDDEN_LAYERS]);
	mxArray *hidden_field = mxGetField(prhs[ANN_STRUCT], 0, fieldnames[HIDDEN]);
	mxArray *outputs_field = mxGetField(prhs[ANN_STRUCT], 0, fieldnames[OUTPUTS]);
	mxArray *total_weights_field = mxGetField(prhs[ANN_STRUCT], 0, fieldnames[TOTAL_WEIGHTS]);
	mxArray *weight_field = mxGetField(prhs[ANN_STRUCT], 0, fieldnames[WEIGHT]);
	mxArray *hidden_act_fnc_field = mxGetField(prhs[ANN_STRUCT], 0, fieldnames[HIDDEN_ACT_FNC]);
	mxArray *output_act_fnc_field = mxGetField(prhs[ANN_STRUCT], 0, fieldnames[OUTPUT_ACT_FNC]);
	
	// integers
	double *tmp;
	tmp = mxGetPr(inputs_field);
	int inputs = (int)tmp[0];

	tmp = mxGetPr(hidden_layers_field);
	int hidden_layers = (int)tmp[0];

	tmp = mxGetPr(hidden_field);
	int hidden = (int)tmp[0];
	
	tmp = mxGetPr(outputs_field);
	int outputs = (int)tmp[0];
	
	tmp = mxGetPr(total_weights_field);
	int total_weights = (int)tmp[0];
	
	tmp = mxGetPr(hidden_act_fnc_field);
	int hidden_act_fnc = (int)tmp[0];
	
	tmp = mxGetPr(output_act_fnc_field);
	int output_act_fnc = (int)tmp[0];

	double *weight = mxGetPr(weight_field);
	/*
	 * AT THIS POINT WE NEED A MATRIX (2D ARRAY)!!!
	 * BUT WE RETURN IN INIT AND TRAIN FUNCTIONS A CELL ARRAY!!!!
	*/
	
	// init genann
	genann *ann = genann_init(inputs, hidden_layers, hidden, outputs);
	
	// change activation function
	if (hidden_act_fnc == sigmoid) {
		ann->activation_hidden = genann_act_sigmoid;
	} else if (hidden_act_fnc == sigmoid_cached) {
		ann->activation_hidden = genann_act_sigmoid_cached;
	} else if (hidden_act_fnc == linear) {
		ann->activation_hidden = genann_act_linear;
	} else if (hidden_act_fnc == threshold) {
		ann->activation_hidden = genann_act_threshold;
	}
	
	if (output_act_fnc == sigmoid) {
		ann->activation_output = genann_act_sigmoid;
	} else if (output_act_fnc == sigmoid_cached) {
		ann->activation_output = genann_act_sigmoid_cached;
	} else if (output_act_fnc == linear) {
		ann->activation_output = genann_act_linear;
	} else if (output_act_fnc == threshold) {
		ann->activation_output = genann_act_threshold;
	}
	
	if (ann->total_weights != total_weights) {
		mexErrMsgTxt("given number of weights doen't match to input data generated number of weights");
	}
	// now overwrite with given weights
	for (n = 0; n < ann->total_weights; ++n) {
		#ifdef DEBUG
			mexPrintf("init weight %d was: %f\n", n, ann->weight[n]);
		#endif
        ann->weight[n] = weight[n];
        #ifdef DEBUG
			mexPrintf("new weight %d was: %f\n", n, ann->weight[n]);
		#endif
    }

    tmp = mxGetPr(prhs[DATA_INPUT]);
    // double data_input = (double)tmp;
    
	// train genann
	// do I need a fifth argument to itereate multiple times to lean this desigred output?
	const double *guess = genann_run(ann, tmp);
	
	plhs[0] = mxCreateStructMatrix(1, 1, 1, retval);
	mxArray *ann_guess = mxCreateCellMatrix(ann->outputs, 1);
	
	for (n = 0; n < ann->outputs; n++) {
		#ifdef DEBUG
			mexPrintf("value %d: %f\n", n, guess[n]);
		#endif
		mxSetCell(ann_guess, n, mxCreateDoubleScalar(guess[n]));
	}
	mxSetFieldByNumber(plhs[0], 0, 0,  ann_guess);
		
	genann_free(ann);

}
