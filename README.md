# mexgenann

This is the most simpliest* and powerful* artificial neural network für GNU Octave (and possible Matlab).

\* Thanks to [genann](https://github.com/codeplea/genann)!

# docs

For [documentation](https://git.osuv.de/m/mexgenann/wiki) the internal [wiki](https://git.osuv.de/m/mexgenann/wiki) of [gitea](https://gitea.io) is used, which is added as a git submodule. So keep a copy of both, you must clone recursively

    git clone --recurse-submodules https://git.osuv.de/m/mexgenann

# build

* use `make 4.4.0` to build against octave 4.4.0 (currently distributed by Arch Linux)
* use `make 4.2.2` to build against octave 4.2.2 (currently distributed by Ubuntu 18.04)

# tests

run `make test` after build successfully.

