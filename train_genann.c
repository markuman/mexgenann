#include <mex.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "genann.h"
#include "mexgenann.h"

//#define DEBUG
//#define DEBUGWEIGHT

typedef enum {
	ANN_STRUCT,
	DATA_INPUT,
	DESIRED_OUTPUT,
	LEARNING_RATE,
	REPEAT_TRAINING
} input_arguments;

char error_string[2000];
int n;
mxArray *ann_weight, *ann_output, *ann_delta;

void mexFunction (int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[]){

	// input handling
	if(nrhs != 5) {
		mexErrMsgTxt("Five inputs are required.");
	}
			
	// input parsing
	mxArray *inputs_field = mxGetField(prhs[ANN_STRUCT], 0, fieldnames[INPUTS]);
	mxArray *hidden_layers_field = mxGetField(prhs[ANN_STRUCT], 0, fieldnames[HIDDEN_LAYERS]);
	mxArray *hidden_field = mxGetField(prhs[ANN_STRUCT], 0, fieldnames[HIDDEN]);
	mxArray *outputs_field = mxGetField(prhs[ANN_STRUCT], 0, fieldnames[OUTPUTS]);
	mxArray *total_weights_field = mxGetField(prhs[ANN_STRUCT], 0, fieldnames[TOTAL_WEIGHTS]);
	mxArray *weight_field = mxGetField(prhs[ANN_STRUCT], 0, fieldnames[WEIGHT]);
	mxArray *output_field = mxGetField(prhs[ANN_STRUCT], 0, fieldnames[OUTPUT]);
	mxArray *hidden_act_fnc_field = mxGetField(prhs[ANN_STRUCT], 0, fieldnames[HIDDEN_ACT_FNC]);
	mxArray *output_act_fnc_field = mxGetField(prhs[ANN_STRUCT], 0, fieldnames[OUTPUT_ACT_FNC]);
	mxArray *delta_field = mxGetField(prhs[ANN_STRUCT], 0, fieldnames[DELTA]);
	
	
	// integers
	double *tmp;
	tmp = mxGetPr(inputs_field);
	int inputs = (int)tmp[0];

	tmp = mxGetPr(hidden_layers_field);
	int hidden_layers = (int)tmp[0];

	tmp = mxGetPr(hidden_field);
	int hidden = (int)tmp[0];
	
	tmp = mxGetPr(outputs_field);
	int outputs = (int)tmp[0];
	
	tmp = mxGetPr(total_weights_field);
	int total_weights = (int)tmp[0];
	
	tmp = mxGetPr(hidden_act_fnc_field);
	int hidden_act_fnc = (int)tmp[0];
	
	tmp = mxGetPr(output_act_fnc_field);
	int output_act_fnc = (int)tmp[0];
	
	double *weight = mxGetPr(weight_field);
	double *output = mxGetPr(output_field);
	double *delta = mxGetPr(delta_field);
	/*
	 * AT THIS POINT WE NEED A MATRIX (2D ARRAY)!!!
	 * BUT WE RETURN IN INIT AND TRAIN FUNCTIONS A CELL ARRAY!!!!
	*/
	
	// init genann
	genann *ann = genann_init(inputs, hidden_layers, hidden, outputs);
	
	// change activation function
	if (hidden_act_fnc == sigmoid) {
		ann->activation_hidden = genann_act_sigmoid;
	} else if (hidden_act_fnc == sigmoid_cached) {
		ann->activation_hidden = genann_act_sigmoid_cached;
	} else if (hidden_act_fnc == linear) {
		ann->activation_hidden = genann_act_linear;
	} else if (hidden_act_fnc == threshold) {
		ann->activation_hidden = genann_act_threshold;
	}
	
	if (output_act_fnc == sigmoid) {
		ann->activation_output = genann_act_sigmoid;
	} else if (output_act_fnc == sigmoid_cached) {
		ann->activation_output = genann_act_sigmoid_cached;
	} else if (output_act_fnc == linear) {
		ann->activation_output = genann_act_linear;
	} else if (output_act_fnc == threshold) {
		ann->activation_output = genann_act_threshold;
	}
	
	if (ann->total_weights != total_weights) {
		mexErrMsgTxt("given number of weights doen't match to input data generated number of weights");
	}
	// now overwrite with given weights
	for (n = 0; n < ann->total_weights; ++n) {
		#ifdef DEBUGWEIGHT
			mexPrintf("init weight %d was: %f\n", n, ann->weight[n]);
		#endif
        ann->weight[n] = weight[n];
        #ifdef DEBUGWEIGHT
			mexPrintf("new weight %d was: %f\n", n, ann->weight[n]);
		#endif
    }
    
    // now overwrite with output of every neuron
    for (n = 0; n < ann->total_neurons; ++n) {
		#ifdef DEBUG
			mexPrintf("init output %d was: %f\n", n, ann->output[n]);
		#endif
        ann->output[n] = output[n];
        #ifdef DEBUG
			mexPrintf("new output %d was: %f\n", n, ann->output[n]);
		#endif
    }
    
    // now overwrite with delta 
    for (n = 0; n < (ann->total_neurons - ann->inputs); ++n) {
		#ifdef DEBUG
			mexPrintf("init delta %d was: %f\n", n, ann->delta[n]);
		#endif
        ann->delta[n] = delta[n];
        #ifdef DEBUG
			mexPrintf("new delta %d was: %f\n", n, ann->delta[n]);
		#endif
    }
    
    tmp = mxGetPr(prhs[LEARNING_RATE]);
    double learning_rate = tmp[0];
    double const *data_input = mxGetPr(prhs[DATA_INPUT]);
    double const *desired_output = mxGetPr(prhs[DESIRED_OUTPUT]);
    tmp = mxGetPr(prhs[REPEAT_TRAINING]);
    //int REPEAT_TRAINING = (int)tmp[0];
    
	// train genann
	genann_train(ann, data_input, desired_output, learning_rate);

	
	
	// save ann in struct
	plhs[0] = mxCreateStructMatrix(1, 11, 11, fieldnames);
	
	// save integer values
	mxSetFieldByNumber(plhs[0], 0, INPUTS,  mxCreateDoubleScalar(inputs));
	mxSetFieldByNumber(plhs[0], 0, HIDDEN_LAYERS,  mxCreateDoubleScalar(hidden_layers));
	mxSetFieldByNumber(plhs[0], 0, HIDDEN,  mxCreateDoubleScalar(hidden));
	mxSetFieldByNumber(plhs[0], 0, OUTPUTS,  mxCreateDoubleScalar(outputs));
	mxSetFieldByNumber(plhs[0], 0, TOTAL_WEIGHTS,  mxCreateDoubleScalar(ann->total_weights));
	mxSetFieldByNumber(plhs[0], 0, TOTAL_NEURONS,  mxCreateDoubleScalar(ann->total_neurons));
	mxSetFieldByNumber(plhs[0], 0, HIDDEN_ACT_FNC,  mxCreateDoubleScalar(hidden_act_fnc));
	mxSetFieldByNumber(plhs[0], 0, OUTPUT_ACT_FNC,  mxCreateDoubleScalar(output_act_fnc));
	
	// save array values	
	#ifdef DEBUGWEIGHT
		mexPrintf("this are the new weights\n");
	#endif
	ann_weight = mxCreateCellMatrix(ann->total_weights, 1);
	for (n = 0; n < ann->total_weights; n++) {
		mxSetCell(ann_weight, n, mxCreateDoubleScalar(ann->weight[n]));
		#ifdef DEBUGWEIGHT
			mexPrintf("%f\n", ann->weight[n]);
		#endif
	}
	mxSetFieldByNumber(plhs[0], 0, WEIGHT,  ann_weight);
	
	#ifdef DEBUG
		mexPrintf("this are the outputs of every neurone\n");
	#endif
	ann_output = mxCreateCellMatrix(ann->total_neurons	, 1);
	for (n = 0; n < ann->total_neurons; n++) {
		mxSetCell(ann_output, n, mxCreateDoubleScalar(ann->output[n]));
		#ifdef DEBUG
			mexPrintf("%f\n", ann->output[n]);
		#endif
	}
	mxSetFieldByNumber(plhs[0], 0, OUTPUT,  ann_output);
	
	#ifdef DEBUG
		mexPrintf("this are the deltas\n");
	#endif
	ann_delta = mxCreateCellMatrix((ann->total_neurons - ann->inputs), 1);
	for (n = 0; n < (ann->total_neurons - ann->inputs); n++) {
		mxSetCell(ann_delta, n, mxCreateDoubleScalar(ann->delta[n]));
		#ifdef DEBUG
			mexPrintf("%f\n", ann->delta[n]);
		#endif
	}
	mxSetFieldByNumber(plhs[0], 0, DELTA,  ann_delta);
	
	
	genann_free(ann);

}
