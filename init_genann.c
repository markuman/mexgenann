#include <mex.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "genann.h"
#include "mexgenann.h"

//#define DEBUG

char error_string[2000];
int inputs, hidden_layers, hidden, outputs, hidden_act_fnc, output_act_fnc;
mxArray *ann_weight, *ann_output, *ann_delta;
int n;

int integer_input(const mxArray *prhs[], input_details type) {
	if ( mxIsDouble(prhs[type]) ) {
		double* data = mxGetPr(prhs[type]);
		return (int)floor(data[0]);
        #ifdef DEBUG
        	mexPrintf("Inputs: %d\n", inputs);
        #endif
      } else {
		  sprintf(error_string, "%s must be an Integer.", fieldnames[type]);
		  mexErrMsgTxt(error_string);
    }
}


void mexFunction (int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[]){

	// input handling
	if(nrhs != 6) {
		mexErrMsgTxt("Four inputs are required.");
	}
		
	// input parsing
	inputs = integer_input(prhs, INPUTS);
	hidden_layers = integer_input(prhs, HIDDEN_LAYERS);
	hidden = integer_input(prhs, HIDDEN);
	outputs = integer_input(prhs, OUTPUTS);
	hidden_act_fnc = integer_input(prhs, HIDDEN_ACT_FNC);
	output_act_fnc = integer_input(prhs, OUTPUT_ACT_FNC);
		
	// init genann
	genann *ann = genann_init(inputs, hidden_layers, hidden, outputs);
	
	// change activation function
	if (hidden_act_fnc == sigmoid) {
		ann->activation_hidden = genann_act_sigmoid;
	} else if (hidden_act_fnc == sigmoid_cached) {
		ann->activation_hidden = genann_act_sigmoid_cached;
	} else if (hidden_act_fnc == linear) {
		ann->activation_hidden = genann_act_linear;
	} else if (hidden_act_fnc == threshold) {
		ann->activation_hidden = genann_act_threshold;
	}
	
	if (output_act_fnc == sigmoid) {
		ann->activation_output = genann_act_sigmoid;
	} else if (output_act_fnc == sigmoid_cached) {
		ann->activation_output = genann_act_sigmoid_cached;
	} else if (output_act_fnc == linear) {
		ann->activation_output = genann_act_linear;
	} else if (output_act_fnc == threshold) {
		ann->activation_output = genann_act_threshold;
	}
		
	// save ann in struct
	plhs[0] = mxCreateStructMatrix(1, 11, 11, fieldnames);
	
	// save integer values
	mxSetFieldByNumber(plhs[0], 0, INPUTS,  mxCreateDoubleScalar(inputs));
	mxSetFieldByNumber(plhs[0], 0, HIDDEN_LAYERS,  mxCreateDoubleScalar(hidden_layers));
	mxSetFieldByNumber(plhs[0], 0, HIDDEN,  mxCreateDoubleScalar(hidden));
	mxSetFieldByNumber(plhs[0], 0, OUTPUTS,  mxCreateDoubleScalar(outputs));
	mxSetFieldByNumber(plhs[0], 0, TOTAL_WEIGHTS,  mxCreateDoubleScalar(ann->total_weights));
	mxSetFieldByNumber(plhs[0], 0, TOTAL_NEURONS,  mxCreateDoubleScalar(ann->total_neurons));
	mxSetFieldByNumber(plhs[0], 0, HIDDEN_ACT_FNC,  mxCreateDoubleScalar(hidden_act_fnc));
	mxSetFieldByNumber(plhs[0], 0, OUTPUT_ACT_FNC,  mxCreateDoubleScalar(output_act_fnc));
	
	// save array values	
	#ifdef DEBUG
		mexPrintf("this are the weights\n");
	#endif
	ann_weight = mxCreateCellMatrix(ann->total_weights, 1);
	for (n = 0; n < ann->total_weights; n++) {
		mxSetCell(ann_weight, n, mxCreateDoubleScalar(ann->weight[n]));
		#ifdef DEBUG
			mexPrintf("%f\n", ann->weight[n]);
		#endif
	}
	mxSetFieldByNumber(plhs[0], 0, WEIGHT,  ann_weight);
	
	#ifdef DEBUG
		mexPrintf("this are the neuron outputs\n");
	#endif
	ann_output = mxCreateCellMatrix(ann->total_neurons, 1);
	for (n = 0; n < ann->total_neurons; n++) {
		mxSetCell(ann_output, n, mxCreateDoubleScalar(ann->output[n]));
		#ifdef DEBUG
			mexPrintf("%f\n", ann->output[n]);
		#endif
	}
	mxSetFieldByNumber(plhs[0], 0, OUTPUT,  ann_output);
	
	#ifdef DEBUG
		mexPrintf("this are the deltas\n");
	#endif
	ann_delta = mxCreateCellMatrix((ann->total_neurons - ann->inputs), 1);
	for (n = 0; n < (ann->total_neurons - ann->inputs); n++) {
		mxSetCell(ann_delta, n, mxCreateDoubleScalar(ann->delta[n]));
		#ifdef DEBUG
			mexPrintf("%f\n", ann->delta[n]);
		#endif
	}
	mxSetFieldByNumber(plhs[0], 0, DELTA,  ann_delta);
	
	/*
	 
	// I'm obviously to stupid for this crappy C interface!
	ann_weight = mxCreateNumericArray(1, (const mwSize *)(long)ann->total_weights, mxUINT32_CLASS, mxREAL);
	double *ann_weight_array = mxGetPr(ann_weight);
	for (n = 0; n < ann->total_weights; n++) {
		ann_weight_array[n] = ann->weight[n];
	}
	*/
	
	genann_free(ann);
}
