## BASIC TEST

testMex = @(x) strcat('MEX FILE: ', x);

ann = init_genann(1, 0, 0, 1, 0, 0);
ann(1).weight = [0, 0];
retval = run_genann(ann, 0);
assert(retval(1).guess{1} == 0.5, 
	testMex('guess must be 0.5'))
	
assert(ann(1).total_weights == 2,
	testMex('total weights must be 2'))
	
retval = run_genann(ann, 1);
assert(retval(1).guess{1} == 0.5, 
	testMex('guess must be 0.5'))

retval = run_genann(ann, 11);
assert(retval(1).guess{1} == 0.5, 
	testMex('guess must be 0.5'))

ann(1).weight = [1, 1];
retval = run_genann(ann, 1);
assert(retval(1).guess{1} == 0.5, 
	testMex('guess must be 0.5'))
	
retval = run_genann(ann, 10);
assert(retval(1).guess{1} > .999, 
	testMex('guess must be nearly 1'))

retval = run_genann(ann, -10);
assert(retval(1).guess{1} < 0.000017, 
	testMex('guess must be nearly 0'))
 
testClassdef = @(x) strcat('CLASSDEF FILE: ', x);

ann = genann(1, 0, 0, 1);
ann.ann(1).weight = [0, 0]; 
retval = ann.run(0);
assert(retval == 0.5, 
	testClassdef('guess must be 0.5'))

ann.ann(1).weight = [1, 1];
retval = ann.run(10);
assert(retval > .999, 
	testClassdef('guess must be nearly 1'))
	
## XOR TEST

ann = genann(2, 1, 2, 1, 'hidden_act_fnc', THRESHOLD, 'output_act_fnc', THRESHOLD);
assert(ann.ann(1).total_weights == 9, 
	testClassdef('total weights must be 9'))

ann.ann(1).weight =  [.5, 1, 1, 1, 1, 1, .5, 1, -1];
 
retval = ann.run([0, 0]);
assert(retval == 0, 
	testClassdef('guess must be 0'))

retval = ann.run([0, 1]);
assert(retval == 1, 
	testClassdef('guess must be 1'))
	
retval = ann.run([1, 0]);
assert(retval == 1, 
	testClassdef('guess must be 1'))
	
retval = ann.run([1, 1]);
assert(retval == 0, 
	testClassdef('guess must be 0'))


## BACKPROP TEST

ann = genann(1, 0, 0, 1);
input = .5;
output = 1;
learning_rate = .5;
first_try = ann.run(input);
ann = ann.train(input, output, learning_rate);
second_try = ann.run(input);

assert(abs(first_try - output) > abs(second_try - output), 
	testClassdef('second try must be closer to the desired output'))

## LEARN XOR

input = [[0, 0]; [0, 1]; [1, 0]; [1, 1]];
desired_output = [0, 1, 1, 0];
learning_rate = 3;
ann = genann(2, 1, 2, 1);
for k = 1:500
	for n = 1:4
		ann = ann.train(input(n,:), desired_output(n), learning_rate);
	end
end

ann = ann.output_activation(THRESHOLD);
retval = ann.run([0, 0]);
assert(retval == 0, 
	testClassdef('learn xor must be 0'))

retval = ann.run([0, 1]);
assert(retval == 1, 
	testClassdef('learn xor must be 1'))
	
retval = ann.run([1, 0]);
assert(retval == 1, 
	testClassdef('learn xor must be 1'))
	
retval = ann.run([1, 1]);
assert(retval == 0, 
	testClassdef('learn xor must be 0'))

