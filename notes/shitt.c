#include "genann.h"
#include <stdio.h>
#include <math.h>
#include <stdlib.h>

int n;


int main(int argc, char *argv[])
{
	genann *ann = genann_init(1, 1, 1, 1);
	
	double input = 0.5;
	double output = 1.0;
  
    for (n = 0; n < ann->total_weights; ++n) {
        ann->weight[n] = 0.5;
    }
    
	for (n = 0; n < ann->total_weights; ++n) {
        printf("%lf\n", ann->weight[n]);
    }
    
    genann_train(ann, &input, &output, .5);

	printf("weights 0.5 changes after one-time training to\n");
    for (n = 0; n < ann->total_weights; ++n) {
        printf("%.*f\n", 15, ann->weight[n]);
    }
    
    genann_train(ann, &input, &output, .5);

	printf("weights 0.5 changes after 2nd-time training to\n");
    for (n = 0; n < ann->total_weights; ++n) {
        printf("%.*f\n", 15, ann->weight[n]);
    }
    
    

    genann_free(ann);
}
