format long g
ann = genann (1, 1, 1, 1);
ann.ann(1).weight(:) = 0.5;
for n = 1:length(ann.ann(1).weight)
	fprintf("%f\n", ann.ann(1).weight(n))
end
fprintf("weights 0.5 changes after one-time training to\n")
ann = ann.train(0.5, 1, .5);
for n = 1:length(ann.ann(1).weight)
	fprintf("%.15f\n", ann.ann(1).weight(n))
end
fprintf("weights 0.5 changes after 2nd-time training to\n")
ann = ann.train(0.5, 1, .5);
for n = 1:length(ann.ann(1).weight)
	fprintf("%.15f\n", ann.ann(1).weight(n))
end
