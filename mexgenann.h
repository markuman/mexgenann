typedef enum {
	sigmoid,
	sigmoid_cached,
	linear,
	threshold
} act_fnc;

typedef enum {
    INPUTS,
    HIDDEN_LAYERS,
    HIDDEN,
    OUTPUTS,
    HIDDEN_ACT_FNC,
    OUTPUT_ACT_FNC,
    TOTAL_WEIGHTS,
    WEIGHT,
    TOTAL_NEURONS,
    OUTPUT,
    DELTA
} input_details;

const char * fieldnames[] =  {
	"inputs",
	"hidden_layers",
	"hidden",
	"outputs",
	"hidden_act_fnc",
	"output_act_fnc",
	"total_weights",
	"weight",
	"total_neurons",
	"output",
	"delta"
};
