.PHONY: help

help: ## This help.
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

.DEFAULT_GOAL := help


4.2.2: ## build for octave version 4.2.2 distributed by ubuntu 18.04
	gcc -fpic -O3 -g -march=native -I/tmp/genann/ -I/usr/include/octave-4.2.2/octave/ -lm  -shared genann.c init_genann.c -o init_genann.mex
	gcc -fpic -O3 -g -march=native -I/tmp/genann/ -I/usr/include/octave-4.2.2/octave/ -lm  -shared genann.c train_genann.c -o train_genann.mex
	gcc -fpic -O3 -g -march=native -I/tmp/genann/ -I/usr/include/octave-4.2.2/octave/ -lm  -shared genann.c run_genann.c -o run_genann.mex


4.4.0: ## build for octave version 4.4.0 distributed by arch linux
	gcc -fpic -O3 -g -march=native -I/tmp/genann/ -I/usr/include/octave-4.4.0/octave/ -lm  -shared genann.c init_genann.c -o init_genann.mex
	gcc -fpic -O3 -g -march=native -I/tmp/genann/ -I/usr/include/octave-4.4.0/octave/ -lm  -shared genann.c train_genann.c -o train_genann.mex
	gcc -fpic -O3 -g -march=native -I/tmp/genann/ -I/usr/include/octave-4.4.0/octave/ -lm  -shared genann.c run_genann.c -o run_genann.mex

test: ## run unit tests after build successfully
	octave-cli --eval 'quit(or(0, mUnittest("test_genann")))'
