classdef genann

    properties
		hidden_act_fnc
		output_act_fnc
		SIGMOID
		SIGMOID_CACHED
		LINEAR
		THRESHOLD
		ann
		repeat_training
    end
    
    methods
        function self = genann(inputs, hidden_layers, hidden, outputs, varargin)  

			p               = inputParser();
			p.CaseSensitive = true;
			p.FunctionName  = 'genann'; 
			% addParamValue is not recommended to use in matlab... however
			% addParameter is not implemented in octave yet, so we've to
			% use it
			p.addParamValue ('hidden_act_fnc',	0, @isnumeric);
			p.addParamValue ('output_act_fnc', 	0, @isnumeric);
			p.addParamValue ('repeat_training', 1, @isnumeric);
			p.parse (varargin{:});

			% init class variables
			self.hidden_act_fnc = p.Results.hidden_act_fnc;
			self.output_act_fnc	= p.Results.output_act_fnc;
			self.repeat_training = p.Results.repeat_training;

			self.SIGMOID = SIGMOID();
			self.SIGMOID_CACHED = SIGMOID_CACHED();
			self.LINEAR = LINEAR();
			self.THRESHOLD = THRESHOLD();

			self.ann = init_genann (inputs, hidden_layers, hidden, outputs, self.hidden_act_fnc, self.output_act_fnc);
			self.ann(1).weight = cell2mat (self.ann(1).weight);
			self.ann(1).output = cell2mat (self.ann(1).output);
			self.ann(1).delta = cell2mat (self.ann(1).delta);
        end

        function self = train(self, data_input, desired_output, learning_rate)
			self.ann = train_genann (self.ann, data_input, desired_output, learning_rate, max(1, self.repeat_training));
			self.ann(1).weight = cell2mat (self.ann(1).weight);
			self.ann(1).output = cell2mat (self.ann(1).output);
			self.ann(1).delta = cell2mat (self.ann(1).delta);
        end

		function retval = run(self, data_input)
			retval = run_genann (self.ann, data_input);
			retval = cell2mat (retval(1).guess);
		end
		
		function self = output_activation (self, this) 
			self.ann(1).output_act_fnc = this;
		end
		
		function self = hidden_activation (self, this)
			self.ann(1).hidden_act_fnc = this;
		end
				
    end
end
